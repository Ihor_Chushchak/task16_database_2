/*1*/
SELECT model,price 
FROM printer
WHERE price IN(
	SELECT MAX(price)
    FROM printer
);
/*2*/
SELECT DISTINCT product.type,laptop.model,laptop.speed 
FROM laptop INNER JOIN product 
ON laptop.model=product.model
AND laptop.speed<=ALL(
	SELECT speed
    FROM pc
);
/*3*/
SELECT maker,printer.price 
FROM product INNER JOIN printer ON product.model=printer.model
WHERE price IN(
	SELECT MIN(price)
    FROM printer
);
/*4*/
SELECT maker,COUNT(maker) 
FROM product
WHERE product.type='PC'AND product.maker=ANY(
	SELECT maker
    FROM product p
    WHERE type='PC' AND p.model!=product.model
);
/*5*/
select maker, avg(hd) 
from pc join product p on pc.model = p.model
where type="PC" 
group by maker 
having maker = ANY(
Select maker 
from product 
where type = "printer");
/*6*/
SELECT COUNT(town_from),date
FROM trip INNER JOIN pass_in_trip ON trip.trip_no=pass_in_trip.trip_no
WHERE town_from='London'
GROUP BY date;
/*7*/
select date, count(*) as "Число рейсів" from Pass_in_trip PIT join Trip T on PIT.trip_no = T.trip_no
where town_to = 'Moscow'
group by date
having count(*) = (select max(total) from (Select count(*) as total from Pass_in_trip PIT join Trip T 
on PIT.trip_no = T.trip_no where town_to = 'Moscow' GROUP BY date) as tmp); 
/*8*/



