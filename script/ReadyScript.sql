SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Прометеус
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Прометеус` ;

CREATE SCHEMA IF NOT EXISTS `Прометеус` DEFAULT CHARACTER SET utf8 ;
USE `Прометеус` ;

-- -----------------------------------------------------
-- Table `Прометеус`.`Group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Group` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Group` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Amount_of_students` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;

INSERT INTO  `Прометеус`.`Group` (Id,Name,Amount_of_students)
VALUES (1,'Group_1',4),
(2,'Group_2',3),
(3,'Group_3',3),
(4,'Group_4',0),
(5,'Group_5',0);

-- -----------------------------------------------------
-- Table `Прометеус`.`Course`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Course` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Course` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Start` DATE NULL,
  `Finish` DATE NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;

INSERT INTO `Прометеус`.`Course`(Id,Name,Start,Finish)
VALUES (1,'Java_developer',20190901,20191216),
(2,'Java_test_automation',20190827,20200105),
(3,'FrontEnd',20200110,20200501),
(4,'.Net',20191005,20200314),
(5,'C#',20200614,20201212);

-- -----------------------------------------------------
-- Table `Прометеус`.`Student`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Student` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Student` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Surname` VARCHAR(45) NULL,
  `Phone_number` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  `Gender` VARCHAR(45) NULL,
  `Rating` DECIMAL NULL,
  `Group_Id` INT NOT NULL,
  `Course_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Student_Group1_idx` (`Group_Id` ASC) VISIBLE,
  INDEX `fk_Student_Course1_idx` (`Course_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Student_Group1`
    FOREIGN KEY (`Group_Id`)
    REFERENCES `Прометеус`.`Group` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Student_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `Прометеус`.`Student`(Id,Name,Surname,Phone_number,Email,Gender,Rating,Group_Id,Course_Id)
VALUES (1,'Ivan','Ivanov','+380506512456','ivan@gmail.com','Man',3.67,1,2),
(2,'Petro','Petrov','+380977562456','perto@gmail.com','Man',4.01,1,2),
(3,'Olha','Andrusiak','+380506512297','olha@gmail.com','Women',3.55,2,1),
(4,'Rostuslav','Andreeew','+380681902454','rostuslav@gmail.com','Man',2.98,1,2),
(5,'Anastasia','Pekarska','+380666872451','anastasia@gmail.com','Woman',4.66,2,1),
(6,'Serhij','Proshchuk','+380501111456','serhij@gmail.com','Man',2.78,2,1),
(7,'Nazar','Diduk','+380336512365','nazar@gmail.com','Man',3.12,3,3),
(8,'Ihor','Chushchak','+380950727050','ihor@gmail.com','Man',4.20,1,2),
(9,'Sophie','Halas','+380657712477','sophie@gmail.com','Woman',4.21,3,3),
(10,'Taras','Normasov','+380506565450','taras@gmail.com','Man',3.00,3,3);
-- -----------------------------------------------------
-- Table `Прометеус`.`Mentor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Mentor` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Mentor` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Surname` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;

INSERT INTO `Прометеус`.`Mentor`(Id,Name,Surname,Email)
VALUES (1,'Andrii','Pavelchak','andrii@ukr.net'),
(2,'Oleh','Veres','oleh@ukr.net'),
(3,'Volodumur','Sukhuj','volodumur@ukr.net'),
(4,'Olha','Bunii','olha@ukr.net'),
(5,'Petro','Andrusiak','petro@ukr.net');
-- -----------------------------------------------------
-- Table `Прометеус`.`Module`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Module` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Module` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Mark` DECIMAL NULL,
  `Course_Id` INT NOT NULL,
  `Student_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Module_Course1_idx` (`Course_Id` ASC) VISIBLE,
  INDEX `fk_Module_Student1_idx` (`Student_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Module_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Module_Student1`
    FOREIGN KEY (`Student_Id`)
    REFERENCES `Прометеус`.`Student` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `Прометеус`.`Module`(Id,Name,Mark,Course_Id,Student_id)
VALUES (1,'Middle_module_test',4.5,2,1),
(2,'Middle_module_test',3.5,2,2),
(3,'Middle_module_test',3.2,2,4),
(4,'Middle_module_test',4.32,2,8),
(5,'Middle_module_test',4.21,1,3),
(6,'Finish_module_test',3.01,1,5),
(7,'Middle_module_dev',3.99,1,6),
(8,'Finish_module_dev',4.06,3,9),
(9,'Finish_module_test',4.98,3,10),
(10,'Middle_module_dev',2.95,3,7);
-- -----------------------------------------------------
-- Table `Прометеус`.`Warning`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Warning` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Warning` (
  `Id` INT NOT NULL,
  `Description` VARCHAR(45) NULL,
  `To` VARCHAR(45) NULL,
  `Date` DATE NULL,
  `Student_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Warning_Student1_idx` (`Student_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Warning_Student1`
    FOREIGN KEY (`Student_Id`)
    REFERENCES `Прометеус`.`Student` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `Прометеус`.`Warning` (Id,`Прометеус`.`Warning`.Description,`Прометеус`.`Warning`.To,`Прометеус`.`Warning`.Date,Student_Id)
VALUES (1,'Does not doing homework','Petro',20191023,2),
(2,'Congratulations, you have passed test','Ivan',20191027,1),
(3,'You failed the last test','Rostuslaw',20191025,4),
(4,'Please send any feedback','Taras',20191028,10),
(5,'Congratulations, you have passed the module','Anastasia',20191023,5),
(6,'Congratulations, you have passed test','Ihor',20191023,8),
(7,'Congratulations, you have passed test','Sophie',20191027,9),
(8,'You failed the last test','Nazar',20191025,7),
(9,'You failed the last module','Serhij',20191028,6),
(10,'Look forward to hearing from you','Olha',20191023,3);

-- -----------------------------------------------------
-- Table `Прометеус`.`Course_has_Mentor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Course_has_Mentor` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Course_has_Mentor` (
  `Id` INT NOT NULL,
  `Course_Id` INT NOT NULL,
  `Mentor_Id` INT NOT NULL,
  INDEX `fk_Course_has_Mentor_Mentor1_idx` (`Mentor_Id` ASC) VISIBLE,
  INDEX `fk_Course_has_Mentor_Course1_idx` (`Course_Id` ASC) VISIBLE,
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_Course_has_Mentor_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Course_has_Mentor_Mentor1`
    FOREIGN KEY (`Mentor_Id`)
    REFERENCES `Прометеус`.`Mentor` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `Прометеус`.`Course_has_Mentor`(Id,Course_Id,Mentor_Id)
VALUES (1,2,1),
(2,2,3),
(3,2,2),
(4,3,4),
(5,3,5),
(6,2,2);

-- -----------------------------------------------------
-- Table `Прометеус`.`Hometask`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Hometask` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Hometask` (
  `Id` INT NOT NULL,
  `Description` VARCHAR(45) NULL,
  `Task` VARCHAR(45) NULL,
  `Mark` DECIMAL NULL,
  `Course_Id` INT NOT NULL,
  `Student_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Hometask_Course1_idx` (`Course_Id` ASC) VISIBLE,
  INDEX `fk_Hometask_Student1_idx` (`Student_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Hometask_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Hometask_Student1`
    FOREIGN KEY (`Student_Id`)
    REFERENCES `Прометеус`.`Student` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `Прометеус`.`Hometask`(Id,Description,Task,Course_Id,Student_Id)
VALUES (1,'GIT','To do something',2,1),
(2,'Collections','To do something',2,2),
(3,'Stream API','To do something',2,4),
(4,'IO/NIO','To do something',2,8),
(5,'String','To do something',1,3),
(6,'Collections','To do something',1,5),
(7,'IO/NIO','To do something',1,6),
(8,'HTML','To do something',3,7),
(9,'CSS','To do something',3,9),
(10,'JS','To do something',3,10);
-- -----------------------------------------------------
-- Table `Прометеус`.`Test`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Прометеус`.`Test` ;

CREATE TABLE IF NOT EXISTS `Прометеус`.`Test` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Mark` DECIMAL NULL,
  `Course_Id` INT NOT NULL,
  `Student_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Module_Course1_idx` (`Course_Id` ASC) VISIBLE,
  INDEX `fk_Module_Student1_idx` (`Student_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Module_Course10`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Module_Student10`
    FOREIGN KEY (`Student_Id`)
    REFERENCES `Прометеус`.`Student` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `Прометеус`.`Test`(Id,Name,Mark,Course_Id,Student_Id)
VALUES (1,'Java_core',4.5,2,1),
(2,'Java_stream',3.5,2,2),
(3,'Java_string',3.2,2,4),
(4,'Java_io/nio',4.32,2,8),
(5,'Java_concurrency',4.21,1,3),
(6,'GIT',3.01,1,5),
(7,'Java_collections',3.99,1,6),
(8,'HTML',4.06,3,9),
(9,'CSS',4.98,3,10),
(10,'JS',2.95,3,7);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
