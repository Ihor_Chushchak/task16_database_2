/*1*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND NOT EXISTS(
	SELECT model
    FROM pc
    WHERE product.model=pc.model);
/*2*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND EXISTS(
	SELECT speed
    FROM pc
    WHERE product.model=pc.model AND speed>=750);
/*3*/
SELECT DISTINCT maker 
FROM product
WHERE type<>'Printer' AND (EXISTS(
	SELECT speed
    FROM pc
    WHERE product.model=pc.model AND speed>=750))
    OR (EXISTS(
    SELECT speed
    FROM laptop
    WHERE product.model=laptop.model AND speed>=750)
);
/*4*/
SELECT maker,model,type
FROM product
WHERE model=(
SELECT model
FROM pc
WHERE price=(SELECT MAX(price) FROM pc)
)
AND
model IN(
SELECT model
FROM product
);
/*5*/
SELECT name,launched,displacement
FROM classes INNER JOIN ships ON classes.class=ships.class
WHERE displacement>35000 AND EXISTS(
	SELECT launched 
    FROM ships
    WHERE launched>1922
);
/*6*/
SELECT class,battle
FROM outcomes INNER JOIN ships
WHERE outcomes.ship = ships.name AND EXISTS(
SELECT class
FROM outcomes
WHERE outcomes.result='sunk');
/*7*/
SELECT DISTINCT maker 
FROM product,printer
WHERE printer.model=product.model AND product.maker = ANY(
SELECT maker 
FROM product,laptop
WHERE product.model = laptop.model);
/*8*/
SELECT DISTINCT maker 
FROM product,laptop
WHERE laptop.model=product.model AND NOT product.maker = ANY(
SELECT maker 
FROM product,printer
WHERE product.model = printer.model);