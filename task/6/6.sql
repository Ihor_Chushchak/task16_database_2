/*1*/
SELECT CONCAT('середня ціна = ',ROUND(AVG(price))) as 'AVG'
FROM laptop;
/*2*/
SELECT CONCAT('код: ',code) as 'Code',
	CONCAT('модель: ',model) as 'Model',
    CONCAT('швидкість: ',speed) as 'Speed',
    CONCAT('оперативка: ',ram) as 'RAM',
    CONCAT('накопичувач: ',hd) as 'HD',
    CONCAT('cd: ',cd) as 'CD',
    CONCAT('ціна: ',price) as 'Price'
FROM pc;
/*3*/
SELECT CONCAT('рік.число_місяця.день ',
YEAR(date),'.',
MONTH(date),'.',
DAY(date)) AS 'Date'
FROM income;
/*4*/
SELECT ship,battle,
	CASE result
		WHEN 'sunk' THEN 'Потоплено'
		WHEN 'OK' THEN 'ֳОкей'
		WHEN 'damaged' THEN 'Пошкоджено'
	END AS 'Результат'
FROM outcomes;
/*5*/
SELECT trip_no,date,id_psg,
	CONCAT('ряд: ',
		SUBSTR(place,1,1)
    ) as row_num,
    CONCAT('місце: ',
		SUBSTR(place,2)
	) as place_num 
FROM pass_in_trip;
/*6*/
SELECT trip_no,ID_comp,plane,
	CONCAT('from ',town_from,' to ',town_to)
    AS destination
FROM trip;
