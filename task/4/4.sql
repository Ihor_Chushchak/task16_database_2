use Labor_SQL;
/*1*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND maker NOT IN(
	SELECT maker
    FROM product
    WHERE type='laptop');
/*2*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND maker<>ALL(
	SELECT maker
    FROM product
    WHERE type='laptop');
/*3*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND NOT maker=ANY(
	SELECT maker
    FROM product
    WHERE type='laptop');
/*4*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND maker IN(
	SELECT maker
    FROM product
    WHERE type='laptop');
/*5*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND NOT maker<>ALL(
	SELECT maker
    FROM product
    WHERE type='laptop');
/*6*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND maker=ANY(
	SELECT maker
    FROM product
    WHERE type='laptop');
/*7*/
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND model IN(
	SELECT model
    FROM pc);
  
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND NOT model<>ALL(
	SELECT model
    FROM pc);
    
SELECT DISTINCT maker 
FROM product
WHERE type='PC' AND model=ANY(
	SELECT model
    FROM pc);
/*8*/
SELECT country,class
FROM classes
WHERE country=ANY(
	SELECT country
    FROM classes);
/*9*/
SELECT ship,battle,battles.date
FROM battles JOIN outcomes 
WHERE result='damaged' AND name=battle AND name IN(
	SELECT name
    FROM battles b1
    WHERE DATEDIFF(b1.date,battles.date)<0);
/*10*/
SELECT COUNT(maker)
FROM product
WHERE type='PC'
GROUP BY maker
HAVING maker='A';
/*11*/
SELECT DISTINCT maker
FROM product
WHERE type='PC' AND NOT model NOT IN(
	SELECT model
    FROM pc);
/*12*/
SELECT DISTINCT model,price
FROM laptop
WHERE price > ANY(
	SELECT price
    FROM pc);

